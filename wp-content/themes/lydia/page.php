<?php
/**
*  PAGE
*  ------
*  Primary Template
*  ------
*  @package lydia
*  @since lydia 1.0
*/
get_header(); ?>

<main class="raised pattern" style="background-image: url(<?php echo get_template_directory_uri();?>/img/star-tile-pattern-grey.svg);">
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
    <article class="primary-cont">
        <div class="container-flex">
            <div class="grid-flex-12 page-head" style="background-image: url(<?php echo the_post_thumbnail_url('full');?>);">
                <div class="page-head-cont">
                    <h1 class="sec-title"><?php the_title() ?></h1>
                    <?php echo the_field('header_content') ?>
                    <h2><?php esc_html_e('Share', 'lydia') ?></h2>
                    <ul class="social icons inline-list">
                            <li><a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&amp;t=<?php the_title(); ?>" target="_blank"><span class="fa fa-3x fa-facebook"></span></a></li>
                        <li><a href="https://twitter.com/intent/tweet?original_referer=<?php the_permalink(); ?>&amp;text=<?php the_title(); ?>&amp;tw_p=tweetbutton&amp;url=<?php the_permalink(); ?>" target="_blank"><span class="fa fa-3x fa-twitter"></span></a></li>
                        <li><a href="mailto:?subject=<?php the_title(); ?>&amp;body=<?php the_permalink(); ?>" target="_blank"><span class="fa fa-3x fa-envelope"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="grid-flex-12 page-content">
                <div class="inner-content">
                    <?php echo the_content() ?>
                </div>
            </div>
        </div>
    </article>
    <?php endwhile; ?>
    <?php else : ?>
        <article class="primary-cont">
            <p><?php esc_html_e('No Content', 'lydia') ?></p>
        </article>
<?php endif; ?>
</main>


<?php get_footer() ?>
