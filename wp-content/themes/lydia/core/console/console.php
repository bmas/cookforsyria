<?php

class Console {
	public static $errorPage, $hideError, $maxDepth = 10, $maxLen = 420;
	protected static $enabled, $debug, $ajax, $dumps = [], $times =[], $warnings = [], $error;

	public static function enable($debug = null) {
		if (self::$enabled) throw new Exception('Console already enabled');
		error_reporting(-1);
		@ini_set('display_errors', false);
		self::$debug = ($debug === null) ? in_array($_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1']) : $debug;
		self::$errorPage = '<title>Server Error</title><center style="color:#aaa;'
			. 'margin-top:46vh;font:24px droid serif">Server Error.';
		if (!function_exists('console')) {
			function console($v = null, $d = null) {
				if (func_get_args()) Console::log($v, $d, 1);
				else return '$this;Console::log(get_defined_vars(),"");';
			}
		}
		if (self::$debug) session_start();
		ob_start();
		register_shutdown_function([__CLASS__, 'render']);
		set_error_handler([__CLASS__, 'errorHandler']);
		set_exception_handler([__CLASS__, 'exceptionHandler']);
		return self::$enabled = true;
	}

	public static function log($v = null, $d = null, $alias = 0) {
		if (!self::$enabled) return;
		$b = debug_backtrace();
		for ($i = $alias; isset($b[$i]) && !isset($b[$i]['file']); $i++);
		$b = sprintf('%s [%s:%d]', self::formatType($v), self::formatPath($b[$i]['file']), $b[$i]['line']);
		self::$dumps[] = [self::formatObject($v), ($d ? $d . ' ' : '') . trim($b)];
	}

	public static function time($name = '') {
		if (!self::$enabled) return;
		self::$times[$name] = microtime(true);
	}

	public static function timeEnd($name = '') {
		if (!self::$enabled) return;
		$time = isset(self::$times[$name]) ? sprintf('%.1f ms', 1000 * (microtime(true) - self::$times[$name])) : null;
		self::log($time, $name);
		unset(self::$times[$name]);
	}

	public static function errorHandler($severity, $message, $file, $line) {
		if (error_reporting() === 0) return;
		$type = in_array($severity, [E_WARNING, E_COMPILE_WARNING, E_USER_WARNING]) ? 'Warning' : 'Notice';
		self::$dumps[] = [sprintf("PHP %s: %s [%s:%d]", $type, $message, self::formatPath($file), $line), 0];
	}

	public static function exceptionHandler($e, $fatal = false) {
		self::$dumps[] = [sprintf("PHP %s: %s\n[%s:%d]\n%s %s",
			$fatal ? 'Fatal Error' : get_class($e), $e->getMessage(), self::formatPath($e->getFile()), $e->getLine(),
			self::formatLines($e->getFile(), $e->getLine()),
			$fatal ? '' : "Traceback:\n" . self::formatPath($e->getTraceAsString())), 1];
	}

	public static function render() {
		$e = error_get_last(); $names = [];
		$toHtml = empty($_SERVER['HTTP_X_REQUESTED_WITH'])
			&& empty($_SERVER['HTTP_X_PHPLOG']) && PHP_SAPI !== 'cli'
			&& !preg_match('#^Content-Type: (?!text/html)#im', implode("\n", headers_list()));
		if ($e && in_array($e['type'], [E_ERROR, E_CORE_ERROR, E_COMPILE_ERROR, E_PARSE])) {
			self::exceptionHandler(new \ErrorException($e['message'].'.', 0, $e['type'], $e['file'], $e['line']), true);
		}
		if (self::$debug) {
			$request = sprintf('%.1fms, %.0fk, %s %s', (microtime(true) - $_SERVER['REQUEST_TIME_FLOAT']) * 1000, memory_get_peak_usage() / 1024, $_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);
			$a = $GLOBALS; unset($a['GLOBALS']);
			self::$dumps = array_merge([[$a, $request]], self::$dumps);
			if (isset($_SESSION['phpconsole'])) {
				self::$dumps = array_merge($_SESSION['phpconsole'], self::$dumps);
				unset($_SESSION['phpconsole']);
			}
			$output = null;
			foreach (self::$dumps as $n => $d) {
				if (is_int($d[1])) {
					self::$error = max(self::$error, $d[1]);
					$output .= sprintf(";log(%s)", json_encode($d[0]));
				} else {
					$output .= sprintf(";log([%s,%s])", json_encode($d[0]), json_encode($d[1]));
				}
			}
		}
		if (http_response_code() >= 300 && http_response_code() < 400) {
			$_SESSION['phpconsole'] = json_decode(json_encode(self::$dumps));
		}
		if (self::$error && !self::$hideError) {
			@header('HTTP/1.1 500 Internal Server Error', true, 500);
			foreach (range(0, ob_get_level()) as $i) ob_end_clean();
			if ($toHtml) {
				print(self::$errorPage);
			}
		}
		if (!$toHtml) {
			header("x-phpconsole: " . json_encode(self::formatScript($output)));
		} elseif (self::$debug) {
			print(self::formatScript($output, true));
		}
	}

	private static function formatObject($var, $level = 0){
		$out = [];
		static $all = [];
		if ($level === 0) $all = [];
		if ((is_array($var) || is_object($var)) && ++$level > self::$maxDepth)
			return is_object($var) ? '{ ... }' : '[ ... ]';
		if (is_array($var)){
			foreach ($var as $key => $value)
				$out[$key . self::formatType($value)] = self::formatObject($value, $level);
			return $out;
		}
		if (is_string($var) && mb_strlen($var) > self::$maxLen)
			return mb_substr($var, 0, self::$maxLen) . '...';
		if (!is_object($var) || $var instanceof Closure) return $var;
		if (in_array($var, $all, true)) return '{ ... }';
		$all[] = &$var;
		foreach ($var as $key => $value)
			$out[$key . self::formatType($value)] = self::formatObject($value, $level);
		$reflect = new \ReflectionClass($var);
		foreach ($reflect->getProperties() as $prop){
			if ($prop->isPublic()) continue;
			$prop->setAccessible(true);
			$vis = $prop->isStatic() ? 'static ' : '';
			$vis .= $prop->isProtected() ? 'protected ' : 'private ';
			$value = $prop->getValue($var);
			$out[$prop->getName() . self::formatType($value, $vis)] = self::formatObject($value, $level);
		}
		return $out;
	}

	private static function formatType($var, $vis = null) {
		if (($c = is_object($var) ? get_class($var) : '') || $vis)
			return sprintf(" (%s)", trim(join(' ', [$vis, $c])));
	}

	private static function formatPath($path) {
		return preg_replace('{^([^/]+\s)?.*/([^/]+/[^/*]+)$}sm', '$1$2', $path);
	}

	private static function formatLines($path, $line, $output = '') {
		$lines = array_slice(explode("\n", file_get_contents($path)), max($line - 5, 0), 6, true);
		foreach ($lines as $n => $code) {
			$output .= sprintf("%s%3s: %s\n", ($n + 1 == $line) ? '*' : ' ', $n + 1, $code);
		}
		return $output;
	}

	private static function formatScript($data, $html = false) {
		$js = "!function(){function c(o){if(o&&/j/.test(typeof o)){o.__proto__=null;for(i in o){o[i]=c(o[i])}}return o}function log(o){console.log(c(o))}$data}()";
		return $html ? "\n\n\n\n<!-- PHP Console -->\n<script>!function(x,p,s,o){s=x[p].send;x[p].send=function(){var _=this.onload;this.onload=function(){eval(eval(this.getResponseHeader('x-phpconsole')));_.apply(this,arguments)};return s.apply(this,arguments)};o=x[p].open;x[p].open=function(){var r=o.apply(this,arguments);this.setRequestHeader('x-phpconsole','1');return r}}(XMLHttpRequest, 'prototype');$js</script>" : $js;
	}
}
