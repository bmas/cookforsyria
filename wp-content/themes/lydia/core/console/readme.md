# console.log() for PHP

# shows PHP dumps and errors in JavaScript console
# works in every modern browser
# supports AJAX requests
# shows all requests when page is redirected

## Usage

```php
<?php
require 'Console.php';

Console::enable();      // show logs on localhost only
Console::enable(true);  // show logs everywhere
Console::enable(false); // hide logs everywhere

Console::log($var, [$title]); // dump variable
console($var, [$title]);      // shorter version
eval(console());              // dump all variables from context

Console::time('name');    // start stopwatches
Console::timeEnd('name'); // stop stopwatches
```
