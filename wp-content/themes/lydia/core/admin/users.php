<?php
/**
*  AUTHOR
*  ------
*  Additional Author / User options
*  ------
*  @package Lydia
*  @since Lydia 1.0
*/

// Add additional Contact info options
function lydia_author_more_details( $contactmethods ) {
  // Add Facebook
  $contactmethods['facebook'] = 'Facebook';
  // Add Twitter
  $contactmethods['twitter'] = 'Twitter';
  // Add Instagram
  $contactmethods['instagram'] = 'Instagram';
  // Add Google +
  $contactmethods['googleplus'] = 'Google +';
  // Add LinkedIn
  $contactmethods['linkedin'] = 'LinkedIn';

return $contactmethods;
}
add_filter('user_contactmethods','lydia_author_more_details', 10, 1);

// Remove Users Select Colour Profile
remove_action("admin_color_scheme_picker", "admin_color_scheme_picker");
