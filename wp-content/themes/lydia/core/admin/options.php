<?php
/**
*  SITE OPTIONS
*  ------
*  Options page to set site variables
*  Requires Advanced Custom Feilds Pro [http://www.advancedcustomfields.com/add-ons/options-page/]
*  ------
*  @package lydia
*  @since lydia 1.0
*/

// NB : Requires Advnaced Custom Fields to be active

// Lydia Options
function lydia_options_page() {
    if (function_exists('acf_add_options_page')) {
        $page = acf_add_options_page(array(
        'page_title' => 'Lydia Options',
        'menu_slug'  => 'lydia-options',
        'capability' => 'edit_posts',
        'position'   => '79',
        'redirect'   => true,
        'post_id'    => 'lydia_options'
        ));
    }
}
add_action('init', 'lydia_options_page');
