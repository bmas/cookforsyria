<?php
/**
*  ADMIN
*  ------
*  Provides customisations to wp admin area.
*  Focused on removing clutter and cleaning up the admin.
*  Changes to WP login screen are to imporve security.
*  ------
*  @package Lydia
*  @since Lydia 1.0
*/

// Additional Admin Styles
function lydia_admin_styles() {
    wp_enqueue_style('bmas-admin-styles', get_template_directory_uri() . '/core/admin/css/admin.css');
}
add_action('admin_enqueue_scripts', 'lydia_admin_styles');

// Add Admin Bar Items
function lydia_admin_bar_additems() {
    global $wp_admin_bar;
    // Add BMAS logo
    $wp_admin_bar->add_node( array(
        'id'    => 'bmas_logo',
        'title' => '<img src="'.get_template_directory_uri().'/core/admin/img/bmas-dashboard-logo.svg"/>',
        'href' => 'https://bmas.agency',
        'parent' => false,
        'meta'  => array('target' => '_blank')
        )
    );
}
add_action('admin_bar_menu', 'lydia_admin_bar_additems', 15);

// Remove Wordpress logo - Admin bar
function lydia_admin_bar_remove_wplogo() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
}
add_action('wp_before_admin_bar_render', 'lydia_admin_bar_remove_wplogo', 0);

// Dashboard - Theme help
function lydia_dashboard_widgets() {
    global $wp_meta_boxes;
    wp_add_dashboard_widget('lydia_help_widget', 'Website Support', 'lydia_help_widget_content');
}
add_action('wp_dashboard_setup', 'lydia_dashboard_widgets');

function lydia_help_widget_content() {
    $site_title = get_bloginfo( 'name' );
    $site_url = get_bloginfo( 'url' );

    echo '<h2>Welcome to '. $site_title .'</h2>';
    echo '<p>This website was developed by <a href="'. AGENCY_LINK .'" target="_blank">'. AGENCY_NAME .'</a><br>';
    echo 'If you require any support or are facing any issues with the website please</p>';
    echo '<a class="button" target="_blank" href="mailto:support@bmas.agency?subject=[Support] '. $site_title . ' : '. $site_url .'">Contact Support</a>';
}

// Remove Dashboard Widgets
function lydia_remove_dashboard_widgets(){
  global$wp_meta_boxes;
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
}
add_action('wp_dashboard_setup', 'lydia_remove_dashboard_widgets');

// Admin Footer
function lydia_change_footer_admin () {
    echo 'Powered by <a href="http://www.wordpress.org" target="_blank">WordPress</a> | Tailored by <a href="'. AGENCY_LINK .'" target="_blank">'.AGENCY_NAME .'</a></p>';
}
add_filter('admin_footer_text', 'lydia_change_footer_admin');

// Add Columns to Post and Page
function lydia_add_admin_columns($columns) {
    return array_merge( $columns,
    array(
        'post_id'              => __('ID', 'lydia'), // Post ID
        'featured_image'       => __('Featured Image', 'lydia'), // Featured Image
        )
    );
}
add_filter('manage_posts_columns' , 'lydia_add_admin_columns', 5);
add_filter('manage_pages_columns' , 'lydia_add_admin_columns', 5);

// Populate Post and Page columns
function lydia_post_and_page_column_id($column_name, $id){

    switch ($column_name) {
        case 'post_id':
                echo $id;
            break;

        case 'featured_image':
            $thumb = get_the_post_thumbnail( $id, array(100,100) );
            if($thumb) {echo $thumb;}
            break;

        default:
            break;
    }
}
add_action( 'manage_posts_custom_column' , 'lydia_post_and_page_column_id', 5, 2);
add_action( 'manage_pages_custom_column' , 'lydia_post_and_page_column_id', 5, 2);

// Remove unused columns from post lists
function lydia_remove_post_columns($defaults) {
    unset($defaults['comments']);
return $defaults;
}
add_filter('manage_posts_columns', 'lydia_remove_post_columns');

// Remove unused columns from pages lists
function lydia_remove_pages_columns($defaults) {
    unset($defaults['comments']);
    unset($defaults['tags']);
return $defaults;
}
add_filter('manage_pages_columns', 'lydia_remove_pages_columns');

// Remove unused Post and Page Edit Boxes
function lydia_remove_meta_boxes() {
// Removes meta boxes from Posts
    remove_meta_box('postcustom','post','normal');
    remove_meta_box('trackbacksdiv','post','normal');
    remove_meta_box('commentstatusdiv','post','normal');
    remove_meta_box('commentsdiv','post','normal');
    remove_meta_box('tagsdiv-post_tag','post','normal');
    remove_meta_box('revisionsdiv','post','normal');
// Removes meta boxes from pages
    remove_meta_box('postcustom','page','normal');
    remove_meta_box('trackbacksdiv','page','normal');
    remove_meta_box('commentstatusdiv','page','normal');
    remove_meta_box('commentsdiv','page','normal');
}
add_action('admin_init','lydia_remove_meta_boxes');

// Remove tags support from posts
function lydia_unregister_tags() {
    unregister_taxonomy_for_object_type('post_tag', 'post');
}
add_action('init', 'lydia_unregister_tags');

// Remove Post by Email settings
add_filter( 'enable_post_by_email_configuration', '__return_false' );
