<?php
/**
*  CONTENT OUTPUT
*  ------
*  Strip out dynamically added tags added to content from the WYSIWYG editor
*  Align post output with common lydia stles.
*  ------
*  @package Lydia
*  @since Lydia 1.0
*/

// Remove auto <br> insertion in content editor
remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );
function lydia_wpautop_nobr( $content ) {
    return wpautop( $content, false );
}
add_filter( 'the_content', 'lydia_wpautop_nobr' );
add_filter( 'the_excerpt', 'lydia_wpautop_nobr' );

// Filter and Replace iframe and img p wappers with div
function replace_ptags_img_and_iframe($content){
    $content = preg_replace('/<p[^>]*>\\s*?(<a .*?><img.*?><\\/a>|<img.*?>)?\\s*<\/p>/', '<div class="post-img">$1</div>', $content);
    return preg_replace('/<p[^>]*>\\s*?(<a .*?><iframe.*?><\\/a>|<iframe.*?>)?\\s*<\/p>/', '<div class="post-video">$1</div>', $content);
}
add_filter('the_content', 'replace_ptags_img_and_iframe');

// Custom Exerpt Length
function lydia_excerpt_length($length) {
    return 150;
}
add_filter('excerpt_length', 'lydia_excerpt_length');

// Exerpt Read More
function lydia_excerpt_read_more($more) {
    global $post;
        return '... <a class="tag-readmore" href="'. get_permalink($post->ID) .'">Read more</a>';
}
add_filter('excerpt_more', 'lydia_excerpt_read_more');

// Disable Emojis
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// Relative URLs - In post/page content
add_action( 'template_redirect', 'lydia_rw_relative_urls' );
function lydia_rw_relative_urls() {
    // Don't do anything if:
    // - In feed
    // - In sitemap by WordPress SEO plugin
    if ( is_feed() || get_query_var( 'sitemap' ) )
        return;
    $filters = array(
        'post_link',
        'post_type_link',
        'page_link',
        'attachment_link',
        'get_shortlink',
        'post_type_archive_link',
        'get_pagenum_link',
        'get_comments_pagenum_link',
        'term_link',
        'search_link',
        'day_link',
        'month_link',
        'year_link',
    );
    foreach ( $filters as $filter )
    {
        add_filter( $filter, 'wp_make_link_relative' );
    }
}

// Remove URL Field from Comment Form
function lydia_disable_comment_url($fields) {
    unset($fields['url']);
    return $fields;
}
add_filter('comment_form_default_fields','lydia_disable_comment_url');
