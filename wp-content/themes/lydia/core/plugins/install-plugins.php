<?php
/**
*  REQUIRED PLUGINS INSTALL
*  ------
*  On Theme activation the required plugins are indicated to be installed.
*  Base plguins add desired functionality for all Lydia Projects.
*  Some plugins are essesntial for theme functionality.
*  Plugins can either be locally packaged or downloaded from an external source.
*  ------
*  @package Lydia
*  @since Lydia 1.0
*/

// Include the lydia_plugin_activation class.
require_once 'class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'lydia_register_required_plugins' );

function lydia_register_required_plugins() {
    $plugins = array(

        // Advanced Custom Fields Pro
        array(
            'name'               => 'Advanced Custom Fields Pro',
            'slug'               => 'advanced-custom-fields-pro',
            'source'             => 'advanced-custom-fields-pro.zip',
            'required'           => true,
            'version'            => '5.3',
            'force_activation'   => false,
            'is_callable'        => 'acf_init'
        ),

        // Yoast SEO
        array(
            'name'               => 'Yoast SEO',
            'slug'               => 'wordpress-seo',
            'source'             => 'wordpress-seo.zip',
            'required'           => true,
            'version'            => '2.3.5',
            'force_activation'   => false,
        ),

        // Google Analytics by Yoast
        array(
            'name'               => 'Google Analytics by Yoast',
            'slug'               => 'google-analytics-for-wordpress',
            'source'             => 'google-analytics-for-wordpress.zip',
            'required'           => true,
            'version'            => '5.4.6',
            'force_activation'   => false,
        ),

        // Force Regenrate Thumbnails
        array(
            'name'               => 'Force Regenerate Thumbnails',
            'slug'               => 'force-regenerate-thumbnails',
            'source'             => 'force-regenerate-thumbnails.zip',
            'required'           => false,
            'version'            => '2.0.5',
        ),

        // Clean Image File Names
        array(
            'name'               => 'Clean Image File Names',
            'slug'               => 'clean-image-filenames',
            'source'             => 'clean-image-filenames.zip',
            'required'           => true,
            'version'            => '1.1.1',
            'force_activation'   => false,
        ),

        // Redirection
        array(
            'name'               => 'Redirection',
            'slug'               => 'redirection',
            'source'             => 'redirection.zip',
            'required'           => true,
            'version'            => '2.4.2',
            'force_activation'   => false,
        ),

        // What the File
        array(
            'name'               => 'What the File',
            'slug'               => 'what-the-file',
            'source'             => 'what-the-file.zip',
            'required'           => false,
            'version'            => '1.5.2',
            'force_activation'   => false,
        ),

        // Woocommerce
        array(
            'name'               => 'Woocommerce',
            'slug'               => 'woocommerce',
            'source'             => 'woocommerce.zip',
            'required'           => false,
            'version'            => '2.5.0',
            'force_activation'   => false,
        ),

    );

//Array of configuration settings.

    $config = array(
        'id'           => 'lydia',                 // Unique ID for hashing notices for multiple instances of lydia.
        'default_path' => get_template_directory() . '/core/plugins/lib/', // Default absolute path to pre-packaged plugins.
        'menu'         => 'lydia-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.


        'strings'      => array(
            'page_title'                      => __( 'Install Required Plugins', 'lydia' ),
            'menu_title'                      => __( 'Install Plugins', 'lydia' ),
            'installing'                      => __( 'Installing Plugin: %s', 'lydia' ), // %s = plugin name.
            'oops'                            => __( 'Something went wrong with the plugin API.', 'lydia' ),
            'notice_can_install_required'     => _n_noop(
                'This theme requires the following plugin: %1$s.',
                'This theme requires the following plugins: %1$s.',
                'lydia'
            ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop(
                'This theme recommends the following plugin: %1$s.',
                'This theme recommends the following plugins: %1$s.',
                'lydia'
            ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop(
                'Sorry, but you do not have the correct permissions to install the %1$s plugin.',
                'Sorry, but you do not have the correct permissions to install the %1$s plugins.',
                'lydia'
            ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop(
                'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
                'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
                'lydia'
            ), // %1$s = plugin name(s).
            'notice_ask_to_update_maybe'      => _n_noop(
                'There is an update available for: %1$s.',
                'There are updates available for the following plugins: %1$s.',
                'lydia'
            ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop(
                'Sorry, but you do not have the correct permissions to update the %1$s plugin.',
                'Sorry, but you do not have the correct permissions to update the %1$s plugins.',
                'lydia'
            ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop(
                'The following required plugin is currently inactive: %1$s.',
                'The following required plugins are currently inactive: %1$s.',
                'lydia'
            ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop(
                'The following recommended plugin is currently inactive: %1$s.',
                'The following recommended plugins are currently inactive: %1$s.',
                'lydia'
            ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop(
                'Sorry, but you do not have the correct permissions to activate the %1$s plugin.',
                'Sorry, but you do not have the correct permissions to activate the %1$s plugins.',
                'lydia'
            ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop(
                'Begin installing plugin',
                'Begin installing plugins',
                'lydia'
            ),
            'update_link'                     => _n_noop(
                'Begin updating plugin',
                'Begin updating plugins',
                'lydia'
            ),
            'activate_link'                   => _n_noop(
                'Begin activating plugin',
                'Begin activating plugins',
                'lydia'
            ),
            'return'                          => __( 'Return to Required Plugins Installer', 'lydia' ),
            'plugin_activated'                => __( 'Plugin activated successfully.', 'lydia' ),
            'activated_successfully'          => __( 'The following plugin was activated successfully:', 'lydia' ),
            'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'lydia' ),  // %1$s = plugin name(s).
            'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'lydia' ),  // %1$s = plugin name(s).
            'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'lydia' ), // %s = dashboard link.
            'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'tgmpa' ),

            'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        ),
    );
    tgmpa( $plugins, $config );
}