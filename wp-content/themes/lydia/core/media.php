<?php
/**
*  MEDIA
*  ------
*  Change and Modify Images and Media
*  ------
*  @package Lydia
*  @since Lydia 1.0
*/

// Set default image attachment display options
function lydia_attachment_display_settings() {
    update_option( 'image_default_align', 'center' );
    update_option( 'image_default_link_type', 'none' );
    update_option( 'image_default_size', 'large' );
}
add_action( 'after_setup_theme', 'lydia_attachment_display_settings' );

 // Get The First Image From a Post
function lydia_first_post_image() {
    global $post, $posts;
    $first_img = '';
    ob_start();
    ob_end_clean();
    if( preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches ) ){
        $first_img = $matches[1][0];
        return $first_img;
    }
}

// Remove gallery shortcode inline styles
add_filter( 'use_default_gallery_style', '__return_false' );

// Media Uploader allow more mime types [SVG]
function lydia_additional_media_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'lydia_additional_media_mime_types');

// Remove the Additional Images added by Srcset
add_filter( 'wp_calculate_image_srcset', '__return_false' );
