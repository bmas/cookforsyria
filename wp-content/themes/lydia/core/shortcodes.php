<?php
/**
*  SHORTCODES
*  ------
*  Add shortcodes for use in both theme templates and WYSIWYG wp editor
*  ------
*  @package Lydia
*  @since Lydia 1.0
*/

// HTML5 Audio
// [audio5 src="http://your-site/videos/your-video.mp4" loop="true" autoplay="autoplay" preload="auto" loop="loop" controls=""]
function lydia_html5_audio($atts, $content = null) {
        extract(shortcode_atts(array(
                "src" => '',
                "autoplay" => '',
                "preload"=> 'true',
                "loop" => '',
                "controls"=> ''
        ), $atts));
        return '<audio src="'.$src.'" autoplay="'.$autoplay.'" preload="'.$preload.'" loop="'.$loop.'" controls="'.$controls.'" autobuffer />';
}
add_shortcode('audio5', 'lydia_html5_audio');
