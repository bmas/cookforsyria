<?php
/**
*  WIDGETS
*  ------
*  Define core widget areas for use with templates and within the WP admin.
*  Adds new widgets alongside wp core widgets, add as required.
*  Dynamic widget areas based on specific rules can be created by adding Woosidebars plugin
*  @link http://www.woothemes.com/woosidebars/
*  ------
*  @package Lydia
*  @since Lydia 1.0
*/

function lydia_register_widget_areas() {

  // Pages Sidebar
  register_sidebar(
     array(
      'name'          => __( 'Pages', 'lydia' ),
      'id'            => 'widget-area-page',
      'description'   => __( 'Sidebar widgets for pages', 'lydia' ),
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h2 class="widget-title">',
      'after_title'   => '</h2>',
     )
  );
  // Footer Widget area
  register_sidebar(
     array(
      'name'          => __( 'Footer', 'lydia' ),
      'id'            => 'widget-area-footer',
      'description'   => __( 'Widget area for Footer', 'lydia' ),
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h2 class="widget-title">',
      'after_title'   => '</h2>',
     )
  );

  // Posts Sidebar
  register_sidebar(
     array(
      'name'          => __( 'Blog', 'lydia' ),
      'id'            => 'widget-area-blog',
      'description'   => __( 'Sidebar widgets for categories and single posts', 'lydia' ),
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h2 class="widget-title">',
      'after_title'   => '</h2>',
     )
  );
}
add_action( 'widgets_init', 'lydia_register_widget_areas' );

// Remove unsed default widgets
function lydia_remove_core_widgets(){
  unregister_widget('WP_Widget_Calendar');
  unregister_widget('WP_Widget_Tag_Cloud');
  unregister_widget('WP_Widget_Recent_Comments');
}
add_action('widgets_init', 'lydia_remove_core_widgets', 1);