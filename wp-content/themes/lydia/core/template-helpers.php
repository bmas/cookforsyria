<?php
/**
*  TEMPLATE HELPERS
*  ------
*  Adds additional classes to relevant html tags to help provoide relevant and dynmaic hooks.
*  ------
*  @package Lydia
*  @since Lydia 1.0
*/

// Add classes to the body tag
function lydia_body_classes( $classes ){

    global $post;

        // Main Page
        if ( is_home() ) {
            $classes[] = 'main';
        }

        // Front Page
        if ( is_front_page() ) {
            $classes[] = 'home';
        }

        // Blog Page
        if ( is_front_page() && is_home() ) {
            $classes[] = 'blog';
        }

        // Page Name
        if ( is_page() ) {
            $page_name = $post->post_name;
            $classes[] = 'page-'.$page_name;
        }

        // Single Post
        if ( is_singular() && ! is_front_page() ) {
            $classes[] = 'singular';
        }

        // Author
        if ( is_author() ) {
            $classes[] = 'author';
        }

        // Multi Author
        if ( is_multi_author() ) {
            $classes[] = 'author-multi';
        }

        // Category Name
        if (is_single() ) {
            foreach((get_the_category($post->ID)) as $category) {
                $classes[] = 'sgl-cat-'.$category->category_nicename;
            }
        }

        // Archive
        if ( is_archive() ) {
            $classes[] = 'archive';
        }

        // Paged Page
        if ( is_paged() ) {
            $classes[] = 'paged';
        }

        // Search Results Page
        if ( is_search() ) {
            $classes[] = 'search-results';
        }

        // Sidebar
        if ( is_active_sidebar( 'widget-area-page', 'widget-area-post' ) && ! is_attachment() && ! is_404() )
            $classes[] = 'sidebar';

        // 404
        if ( is_404()) {
            $classes[] = 'error-404';
        }

        // Password Protected
        if ( post_password_required()) {
            $classes[] = 'password-protected';
        }

        // Attachment
        if ( is_attachment()) {
            $classes[] = 'attachment';
        }

         // Attachment Image
        if ( wp_attachment_is_image()) {
            $classes[] = 'attachment-img';
        }

        if ( isset( $post ) ) {
            $classes[] = $post->post_type . '-' . $post->post_name;
        }

    return $classes;
}
add_filter( 'body_class', 'lydia_body_classes' );

// Reduce nav classes, leaving only 'current-menu-item'
function lydia_nav_class_filter( $var ) {
    foreach ($var as $key => $value) {
        if (preg_match('/^(menu-item-?.*|page_item|page-item-.*|current_page_item)$/', $value )) {
            unset($var[$key]);
        }
    }
    return $var;
}
add_filter('nav_menu_css_class', 'lydia_nav_class_filter', 100, 1);

// Add page slug as nav IDs
function lydia_nav_add_id( $id, $item ) {
    return 'nav-'.strtolower( str_replace( ' ','-',$item->title ) );
}
add_filter( 'nav_menu_item_id', 'lydia_nav_add_id', 10, 2 );

// Dynamic Copyright - [ echo comicpress_copyright(); ]
function lydia_dynamic_copyright() {
    global $wpdb;
    $copyright_dates = $wpdb->get_results("
        SELECT
        YEAR(min(post_date_gmt)) AS firstdate,
        YEAR(max(post_date_gmt)) AS lastdate
        FROM
        $wpdb->posts
        WHERE
        post_status = 'publish'
        ");
    $output = '';
    if($copyright_dates) {
        $copyright = "&copy; " . $copyright_dates[0]->firstdate;
        if($copyright_dates[0]->firstdate != $copyright_dates[0]->lastdate) {
            $copyright .= '-' . $copyright_dates[0]->lastdate;
        }
        $output = $copyright;
    }
    return $output;
}

// Fixed Copyright
function lydia_fixed_copyright() {
    $fromYear = 2016;
    $thisYear = (int)date('Y');
    $output = $fromYear . (($fromYear != $thisYear) ? '-' . $thisYear : '');

    return $output;
}

// Add Expert Class to the_expert
function lydia_add_class_to_excerpt( $excerpt ) {
    return str_replace('<p', '<p class="excerpt"', $excerpt);
}
add_filter( "the_excerpt", "lydia_add_class_to_excerpt" );
