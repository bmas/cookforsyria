<?php
/**
*  SCRIPTS
*  ------
*  Enqueue Scripts for both wp_head and wp_footer
*  ------
*  @package Lydia
*  @since Lydia 1.0
*/

// Add base stylesheet to wp_head
function lydia_enqueue_style_scripts() {

    // Main Stylesheet
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', 'style');

    // jQuery and addons
    wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js", false, null);
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui-accordion');
    wp_enqueue_script('jquery-effects-core');

    // jQuery Plugins
    wp_enqueue_script( 'lydia-vendor', get_template_directory_uri() .'/js/jquery.vendor.js', 'jquery', '1.0', true );

    // Simple Ajax [Will BMAS]
    wp_enqueue_script( 'lydia-ajax', get_template_directory_uri() .'/js/ajax.wp.js', 'jquery', '1.0', true );

    // Owl Carousel
    wp_register_script( 'lydia-owlcarousel', get_template_directory_uri() . '/js/owl.carousel.min.js', 'jquery', '2.0.0', true );
    wp_enqueue_script('lydia-owlcarousel');

    // Custom jQuery Scripts
    wp_enqueue_script( 'lydia-scripts', get_template_directory_uri() .'/js/jquery.lydia.js', 'jquery', '1.0', true );
}
if (!is_admin()) {
    add_action( 'wp_enqueue_scripts', 'lydia_enqueue_style_scripts', 10 );
}

// Lydia Google Fonts
function lydia_google_fonts() {
    $query_args = array(
        'family' => 'Montserrat:400,700',
    );
    wp_enqueue_style( 'google_fonts', add_query_arg( $query_args, "//fonts.googleapis.com/css" ), array(), null );
            }
add_action('wp_enqueue_scripts', 'lydia_google_fonts', 1);
