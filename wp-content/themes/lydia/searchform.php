<?php
/**
*  SEARCH FORM
*  ------
*  SEARCH FORM
*  ------
*  @package lydia
*  @since lydia 1.0
*/
?>
<form class="wrapper search-bar fluid" action="/" method="get">
    <input type="text" name="s" placeholder="<?php esc_attr_e('Search ...', 'lydia') ?>" id="search" value="<?php the_search_query(); ?>" />
</form>
