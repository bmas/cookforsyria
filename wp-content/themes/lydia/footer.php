<?php
/**
*  FOOTER
*  ------
*  Primary Footer Template
*  ------
*  @package lydia
*  @since lydia 1.0
*/
?>

<footer class="bedrock" role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
      <div class="container-flex footer-center">
        <section class="grid-flex-8">
        <img class="footer-logo" src="<?php echo get_template_directory_uri(); ?>/img/cook-for-syria-logo-long-w.svg" alt="<?php esc_attr_e('Cook for Syria', 'lydia') ?>">
                <p class="footer-byline"><?php echo bloginfo('description') ?></p>

                <ul class="social icons inline-list">
                    <li><a href="https://www.instagram.com/CookForSyria/" target="_blank"><span class="fa fa-3x fa-instagram"></span></a></li>
                    <li><a href="https://twitter.com/CookForSyria" target="_blank"><span class="fa fa-3x fa-twitter"></span></a></li>
                </ul>
        </section>

        <section class="grid-flex-4">
            <?php wp_nav_menu( array(
                    'theme_location'  => 'footer',
                    'menu'            => 'footer',
                    'container'       => 'nav',
                    'container_class' => 'footer-menu',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s no-bullet">%3$s</ul>',
                    'depth'           => 0,
                ));
            ?>
        </section>

        <section class="grid-flex-12 text-center">
            <a href="#" class="back-top"><?php _e( 'Back to top', 'lydia' ); ?> <span class="fa fa-chevron-up"></span></a>
            <?php get_template_part('templates/lydia', 'siteinfo'); ?>
        </section>
    </div>
</footer>

<div class="modal" style="visibility: hidden;" id="modal-signup">
    <div class="modal-inner">
        <div class="container collapse">
            <div class="modal-img grid-6-ng">
                 <?php
                 $image = get_field('modal_image', 'lydia_options');
                 if( !empty($image) ): ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                 <?php endif; ?>
            </div>
            <div class="modal-content grid-6-ng">
                <span class="fa fa-close modal-close"></span>
                <h4 class="capitals"><?php the_field('modal_title', 'lydia_options'); ?></h4>
                <?php the_field('modal_content', 'lydia_options'); ?>
                <?php //get_template_part('templates/template', 'book-signup'); ?>
            </div>
        </div>
    </div>
</div>

<?php wp_footer() ?>
</body>
</html>
