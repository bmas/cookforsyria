<?php
/**
*  Single Template
*  ------
*  Primary Single Template
*  ------
*  @package lydia
*  @since lydia 1.0
*/
get_header(); ?>

<main class="raised pattern" style="background-image: url(<?php echo get_template_directory_uri();?>/img/star-tile-pattern-grey.svg);">
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
    <article class="primary-cont">
        <div class="container-flex">
            <div class="grid-flex-6 page-head">
                <?php
                $images = get_field('recipe_gallery');
                if( $images ): ?>
                    <div id="slider" class="recipe-gallery">
                        <div class="post-slider-single-fade">
                            <?php foreach( $images as $image ): ?>
                                <div>
                                    <img src="<?php echo $image['sizes']['masonry-square']; ?>" alt="<?php echo $image['alt']; ?>" />
                                    <p><?php echo $image['caption']; ?></p>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <span style="margin-top: 10px" class="small-caps"><?php esc_html_e('Photography By', 'lydia') ?> : <?php the_field('photo_credit');  ?></span>
            </div>
            <div class="grid-flex-6 page-content">
                <div class="page-head-cont">
                    <a class="h3 small-caps" href="<?php echo get_post_type_archive_link( 'recipe' ); ?>"><?php esc_html_e('View all Recipes', 'lydia') ?></a>
                    <hr>
                    <h1 class="sec-title"><?php the_title(); ?></h1>
                    <h2 class="dish-by"><span class="fa fa-instagram color-accent"></span><?php esc_html_e('Dishes Created By', 'lydia') ?></h2>
                    <p class="chef-link"><a href="<?php the_field('chef_instagram_link'); ?>" target="_blank"><?php the_field('chef_name'); ?></a></p>
                    <hr>
                    <p class="recipe-sh-des"><?php the_field('recipe_short_description') ?></p>
                    <ul class="inline-list icons recipe-details">
                        <?php
                        $post_object = get_field('participant_name');
                        if( $post_object ):
                            $post = $post_object;
                            setup_postdata( $post );
                            ?>
                            <li><span class="fa fa-map-marker"></span><?php the_title() ?></li>
                            <?php wp_reset_postdata(); ?>
                        <?php endif; ?>
                        <li><span class="fa fa-users"></span><?php the_field('serves') ?></li>
                    </ul>
                    <hr>
                    <div class="share-rec">
                        <h2>Share</h2>
                        <ul class="social icons inline-list">
                            <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_site_url(); ?><?php the_permalink(); ?>" target="_blank"><span class="fa fa-2x fa-facebook"></span></a></li>
                            <li><a href="https://twitter.com/intent/tweet?original_referer=<?php echo get_site_url(); ?><?php the_permalink(); ?>&amp;text=<?php the_title(); ?>&amp;tw_p=tweetbutton&amp;url=<?php the_permalink(); ?>" target="_blank"><span class="fa fa-2x fa-twitter"></span></a></li>
                            <li><a href="mailto:?subject=<?php the_title(); ?>&body=<?php echo get_site_url(); ?><?php the_permalink(); ?>" target="_blank"><span class="fa fa-2x fa-envelope"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-flex instructions">
            <div class="grid-flex-6 ingredients-list">
            <h2><?php esc_html_e('Ingredients', 'lydia') ?></h2>
            <hr>
            <?php if( have_rows('ingredients_list') ): ?>

                <?php while( have_rows('ingredients_list') ): the_row();
                    // vars
                    $ingredient_group = get_sub_field('ingredient_group');
                    $ingredient_item = get_sub_field('ingredient_item');
                    ?>
                    <h3><?php echo $ingredient_group; ?></h3>
                    <ul class="ingredient-list no-bullet">
                        <?php foreach($ingredient_item as $item) : ?>
                            <li class=""><?php echo $item['ingredient_item']; ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endwhile; ?>

            <?php endif; ?>

            </div>
            <div class="grid-flex-6 method-steps">
                <h2><?php esc_html_e('Cooking Instructions', 'lydia') ?></h2>
                <hr>
                <?php if( have_rows('method_steps') ): ?>
                    <ol class="method-list">
                    <?php while( have_rows('method_steps') ): the_row();
                        // vars
                        $method_step = get_sub_field('method_item');
                        ?>
                            <li class=""><?php echo $method_step ?></h2>
                    <?php endwhile; ?>
                    </ol>
                <?php endif; ?>
            </div>
        </div>
        <p class="text-center"><a class="btn outline" href="<?php echo get_post_type_archive_link( 'recipe' ); ?>"><?php esc_html_e('View all Recipes', 'lydia') ?></a></p>
    </article>
    <?php endwhile; ?>
    <?php else : ?>
        <article class="primary-cont">
            <p><?php esc_html_e('No Content', 'lydia') ?></p>
        </article>
<?php endif; ?>
</main>


<?php get_footer() ?>
