<?php
/**
*  FAQ PAGE
*  ------
*  Primary Template
*  ------
*  @package lydia
*  @since lydia 1.0
*/
get_header(); ?>

<main class="raised pattern" style="background-image: url(<?php echo get_template_directory_uri();?>/img/star-tile-pattern-grey.svg);">
    <article class="primary-cont">
        <div class="container-flex">
            <div class="grid-flex-12 page-head" style="background-image: url(<?php the_field('recipe_archive_featured_image', 'lydia_options');?>);">
                <div class="page-head-cont">
                    <h1 class="sec-title"><span class="top-title"><?php the_field('recipe_archive_top_title', 'lydia_options');?></span><br><?php post_type_archive_title(); ?></h1>
                    <?php the_field('recipe_archive_description', 'lydia_options');?>
                    <h2><?php esc_html_e('Share', 'lydia') ?></h2>
                    <ul class="social icons inline-list">
                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_site_url(); ?><?php echo get_post_type_archive_link( 'recipe' ); ?>" target="_blank"><span class="fa fa-2x fa-facebook"></span></a></li>
                        <li><a href="https://twitter.com/intent/tweet?original_referer=<?php echo get_site_url(); ?><?php echo get_post_type_archive_link( 'recipe' ); ?>&amp;text=<?php post_type_archive_title(); ?>&amp;tw_p=tweetbutton&amp;url=<?php echo get_site_url(); ?><?php echo get_post_type_archive_link( 'recipe' ); ?>" target="_blank"><span class="fa fa-2x fa-twitter"></span></a></li>
                        <li><a href="mailto:?subject=<?php post_type_archive_title(); ?>&amp;body=<?php echo get_site_url(); ?><?php echo get_post_type_archive_link( 'recipe' ); ?>" target="_blank"><span class="fa fa-2x fa-envelope"></span></a></li>
                    </ul>
                </div>
            </div>
            </div>

            <?php if (have_posts()) : ?>
                <div class="container-flex page-content">
                    <?php if (is_post_type_archive('recipe')) { ?>
                        <h2 class="filter-title"><?php esc_html_e('Filters', 'lydia') ?></h2>
                        <ul class="filters">
                            <li>
                                <button data-filter="courses" data-name="all"><?php esc_html_e('All recipies', 'lydia') ?></button>
                            </li>
                            <?php foreach (get_terms('course') as $course) { ?>
                                <li>
                                    <button data-filter="courses" data-name="<?php echo htmlspecialchars(strtolower($course->name)); ?>">
                                        <?php echo $course->name; ?>
                                    </button>
                                </li>
                            <?php } ?>
                            <?php foreach ([
                              'headline' => esc_html__('Headline'),
                              'cookbook' => esc_html__('Cookbook'),
                              'restaurant' => esc_html__('Restaurant'),
                              'traditional' => esc_html__('Traditional')
                            ] as $chef_type => $chef_type_label) { ?>
                                <li>
                                    <button data-filter="chef_types" data-name="<?php echo $chef_type; ?>">
                                        <?php echo $chef_type_label; ?>
                                    </button>
                                </li>
                            <?php } ?>
                            <li>
                                <input type="text" name="search-filter" placeholder="<?php esc_attr_e('Search', 'lydia') ?>">
                            </li>
                        </ul>
                    <?php } ?>
            <?php while (have_posts()) : the_post(); ?>
                <?php 
                    $courses = get_the_terms(get_the_ID(), 'course');
                    $coursesSerialize = [];
                    foreach ($courses as $course) {
                        $coursesSerialize[] = strtolower($course->name);
                    }
                ?>
                <article class="grid-flex-3 headline-card"
                    data-courses="<?php echo htmlspecialchars(implode(', ', $coursesSerialize)); ?>"
                    data-chef_types="<?php echo htmlspecialchars(implode(', ', get_field('chef_type'))); ?>"
                    data-title="<?php echo htmlspecialchars(strtolower(get_the_title())); ?>">
                    <a href="<?php the_permalink() ?>">
                    <h3 class="card-name"><?php the_title(); ?></h3>
                        <div class="card-img">
                            <?php the_post_thumbnail('masonry-square') ?>
                            <div class="card-info">
                                <p><?php the_field('chef_name'); ?></p>
                            </div>
                        </div>
                    </a>
                </article>
                <?php endwhile; ?>
            </div>
                <?php else : ?>
                    <div class="inner-content">
                        <p><?php esc_html_e('No Content', 'lydia') ?></p>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </article>

</main>


<?php get_footer() ?>
