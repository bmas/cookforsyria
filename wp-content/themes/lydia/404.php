<?php
/**
*  INDEX
*  ------
*  Primary Template
*  ------
*  @package lydia
*  @since lydia 1.0
*/
get_header(); ?>

<main class="raised">
    <article class="primary-content" style="padding: 10%;">
        <section class="error-404">
            <h1 class="page-title">404</h1>
            <p><?php esc_html_e('The page you were looking for appears to have been moved, deleted, or does not exist.', 'lydia') ?></p>
            <div class="btn outline">
                <a href="<?php echo home_url();?>" title="<?php esc_attr__('Return to Home Page', 'lydia') ?>" alt="<?php echo home_url();?>"><?php esc_html_e('Return to Home Page', 'lydia') ?></a>
            </div>
        </section>
    </article>
</main>

<?php get_footer() ?>
