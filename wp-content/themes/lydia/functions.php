<?php
/**
*  Lydia Functions and Definitions
*  ------
*  @package Lydia
*  @since Lydia 1.0
*  @author Alexander Hawkings-Byass (bmas.agency)
 */

/**
*  LYDIA INITALIZATION SETUP
*  ------
*  Sets up theme defaults and registers support for various WordPress features.
*  Note that this function is hooked into the after_setup_theme hook, which
*  runs before the init hook. The init hook is too late for some features, such
*  as indicating support for post thumbnails.
*  ------
*  @package Lydia
*  @since Lydia 1.0
*/

if ( ! function_exists( 'lydia_setup' ) ) :

function lydia_setup() {

    // Make theme available for translation, languages can be filed in the /languages/ directory
    load_theme_textdomain( 'lydia', get_template_directory() . '/languages' );

    // Title tag added dynamically through WP and not harcoded in template
    add_theme_support( 'title-tag' );

    // Enable post thumbnails
    add_theme_support( 'post-thumbnails' );
    // Register Post Sizes - Additional Sizes can be added as required per theme
    // Featured Images use a 16:9 aspect ratio
    add_image_size( 'featured-banner', 1100, 619,  array( 'center', 'center' ) );
    add_image_size( 'featured-blog', 690, 388, true );
    add_image_size( 'masonry-square', 600, 600, true );
    add_image_size( 'masonry', 330, 9999 );
    add_image_size( 'square', 330, 330, true );
    add_image_size( 'rectangle', 330, 220, true );
    add_image_size( 'thumbs', 100, 70, true );

    // Register menus for use with wp_nav_menu()
    register_nav_menus( array(
        'header' => __( 'Header Menu', 'lydia' ),
        'footer' => __( 'Footer Menu', 'lydia' ),
    ) );

    // Add Theme support for HTML5 elements
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );

    // Add Theme support for post formats
    add_theme_support( 'post-formats', array(
        'aside',
        'image',
        'video',
        'quote',
        'link',
        'gallery',
        'status',
        'audio',
        'chat',
    ) );
}

endif; // lydia_setup
add_action( 'after_setup_theme', 'lydia_setup' );

// Set content width in px
// Priority 0 to make it available to lower priority callbacks
function lydia_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'lydia_content_width', 690 );
}
add_action( 'after_setup_theme', 'lydia_content_width', 0 );

/**
*  DEBUGING
*  ------
*  PHP Debuging for use during development.
*/

//Console for PHP DEBUG
include_once 'core/console/console.php';
if (!is_admin()) {
    Console::enable();
}

/**
*  LIBRARIES
*  ------
*  Registers and Enuqeue base and external styles, js and jQuery.
*  By adding scripts via enquee WP can load the scripts in the correct order and reslove conflicts,
*  also ensuring that any additional 3rd party plugins are aware of the script stack in place,
*  and avoid loading duplicates.
*/

// Enqueue style and js scripts
include_once ( 'core/scripts.php' );
include_once ('inc/ajax.php');
include_once ('vendor/autoload.php');

/**
*  ADMIN
*  ------
*  Provides customisations to wp admin area.
*  Focused on removing clutter and cleaning up the admin.
*  Changes to WP login screen are to imporve security.
*/

// Wordpress Admin customisations
include_once ( 'core/admin/admin.php' );
// WP Login
include_once ( 'core/admin/login.php' );
// Security
include_once ( 'core/admin/security.php' );
// Author
include_once ( 'core/admin/users.php' );

/**
*  OPTIONS
*  ------
*  Options page for theme settings and for setting client variables
*  Requires Advanced Custom Fields
*/

// Options
include_once ( 'core/admin/options.php' );

/**
*  PLUGINS
*  ------
*  Register and install required plugins.
*  Not all of the included plugins are required for theme functionality.
*  However, most are highly recommended to ensure necessary features are included.
*  Additional Plugin modifications are supplied on a per vendor basis.
*  These will not be required for all projects, so remove as required.
*/

// Install requried plugins
// Plugins are either bundled or downloadable from online repositories
include_once ( 'core/plugins/install-plugins.php' );

/**
*  WP CORE MODIFICATIONS
*  ------
*  Modifies core wordpress behavoiur and adds/unlocks additional functionality.
*  Makes changes to the wp WYSIWYG editor and its output.
*/

// WP contnet editor and output customisations
include_once ( 'core/content-output.php' );
// Images and Media
include_once ( 'core/media.php' );

/**
*  WIDGETS
*  ------
*  Define core widget areas for use with templates and within the WP admin.
*  Adds new widgets alongside wp core widgets, add as required.
*/

// Register Widget Areas
include_once ( 'core/widget-areas.php' );
// Add Widgets
include_once ( 'core/widgets/breadcrumbs.php' );
//include_once ( 'core/widgets/{Insert-File-Name}.php' );

/**
*  TEMPLATE HELPERS AND COMPONENTS
*  ------
*  Adds additional classes to relevant html tags to help provoide relevant and dynmaic hooks.
*  Cleans up dynamic wp core component classes to ensure leaner code and reduced bloated html.
*  Adds Shortcodes for use in templates and post editor
*/

// Additional Classes
include_once ( 'core/template-helpers.php' );
// Favicons
include_once ( 'inc/favicons.php' );
// Shortcodes
include_once ( 'core/shortcodes.php' );
// Instagram
include_once ('vendor/instagram.php');

/**
* PLUGIN OVERRIDES
* ------
* Modifies core behavour of 3rd party plugins.
*/

// Woocommerce
include_once ( 'core/vendor/woocommerce.php' );

/**
*  TEMPORARY & CLIENT
*  ------
*  Temporary functions that are not required permanently, such as a hotfix for bugs.
*  Other extra scripts that do not fit within the core sections.
*  Client site functions as per the site requirements
*/

// Custom Posts and Taxonomies
/* DO NOT ACTIVATE UNTIL CONFIGURED */
require_once ( 'inc/custom-posts.php');

// Client Specfic Functions
include_once ( 'inc/client-functions.php' );
// Shame Functions
include_once ( 'inc/shame.php' );

