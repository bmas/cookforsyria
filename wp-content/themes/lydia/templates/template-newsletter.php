<?php
/**
*  TEMPLATE PART: NEWSLETTER SIGNUP
*  ------
*  MailChimp Newsletter Signup
*  ------
*  @package lydia
*  @since lydia 1.0
*/
?>
<form class="newsletter v-spacing" action="<?php the_field('mailchimp_url', 'lydia_options'); ?>" method="post" name="mc-embedded-subscribe-form" target="_blank" novalidate>
        <label class="capitals" for="mce-EMAIL"><?php esc_html_e('Sign up to our Newsletter', 'lydia') ?></label>
        <div class="container">
            <div class="container" style="margin-bottom: 1.69492%;">
                <input class="grid-6" type="text" placeholder="<?php esc_attr_e('First Name', 'lydia') ?>" value="" name="FIRSTNAME">
                <input class="grid-6" type="text" placeholder="<?php esc_attr_e('Last Name', 'lydia') ?>" value="" name="LASTNAME">
            </div>
            <input class="grid-12" type="email" placeholder="<?php esc_attr_e('Email Address', 'lydia') ?>" value="" name="EMAIL">
        </div>
        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
        <input class="visually-hidden" type="text" name="b_c067b3180167bf6f148939989_d22c98b998" tabindex="-1" value="">
        <input class="w-25 btn outline" type="submit" value="<?php esc_attr_e('Subscribe', 'lydia') ?>" name="subscribe">
</form>
