<?php
/**
*  HOME PAGE BLOCK: HEADLINE CHEFS
*  ------
*  Home page sections
*  ------
*  @package lydia
*  @since lydia 1.0
*/
?>

<div class="intro centred">
<h2 class="title"><span class="fa fa-star"></span><br><?php esc_html_e('Headline Chefs', 'lydia') ?></h2>
    <?php the_field('headline_chefs_intro') ?>
</div>
<div class="container-flex chef-highlight">
    <?php $post_query = new WP_Query(
    array(
        'post_type' => 'recipe',
        'post_status' => 'publish',
        'order'   => 'ASC',
        'meta_query' => array(
            array(
                'key' => 'chef_type', // name of custom field
                'value' => '"headline"', // matches exactly "red"
                'compare' => 'LIKE'
            )
        ),
        'posts_per_page' => 7
        )
    );

    $names = [];

    while ($post_query->have_posts()) : $post_query->the_post(); ?>
    <?php if(!in_array(get_field('chef_name'), $names)) { ?>
        <?php $names[] = get_field('chef_name'); ?>
        <div class="grid-flex-4 headline-card">
            <a href="<?php the_permalink() ?>">
            <h3 class="card-name"><?php the_field('chef_name'); ?></h3>
            <div class="card-img">
                <?php
                $image = get_field('chef_image');
                $size = 'masonry-square';
                if( $image ) {
                    echo wp_get_attachment_image( $image, $size, "", array( 'class' => 'chef-img' ) );
                }
                ?>
                <div class="card-info">
                    <p class="cta"><?php esc_html_e('View Recipe', 'lydia') ?></p>
                    <ul class="icons">
                        <li>
                            <svg xmlns="http://www.w3.org/2000/svg" width="148.9" height="146.3" viewBox="0 0 148.9 146.3">
                              <path d="M147.47,83.62q-.23,1.29-.5,2.57A4.93,4.93,0,0,1,142.12,90H6.78a4.93,4.93,0,0,1-4.85-3.83q-.27-1.28-.5-2.57a10.54,10.54,0,0,1,10.41-12.3H137.06A10.54,10.54,0,0,1,147.47,83.62ZM34.78,140.36a4.57,4.57,0,0,0,4.62,4.94h70.19c2.52,0,4.56-2,4.56-5.08,0-.73-.36-1.4-.36-2.51a7.18,7.18,0,0,1,2.55-5.47,74.38,74.38,0,0,0,25.09-29.38,2.47,2.47,0,0,0-2.25-3.51H9.72a2.47,2.47,0,0,0-2.25,3.51,74.34,74.34,0,0,0,25.1,29.47c1.38.95,2.5,3.45,2.5,5.57C35.07,138.92,34.78,139.17,34.78,140.36ZM82.5,2.53a1.17,1.17,0,0,0-1.92-1.21,24.53,24.53,0,0,0-9.76,20c0,13.79,14.65,14.66,14.65,25.53,0,3.82-1.92,9-3.39,12.34a1.25,1.25,0,0,0,1.83,1.55c4.49-3,11.76-9.56,11.76-21.15,0-12.07-14.93-16.43-14.93-27.22A33.94,33.94,0,0,1,82.5,2.53ZM59.87,22a1.36,1.36,0,0,0-2.42-1c-2,2.56-4.21,6.24-4.21,10.19,0,9.14,9.21,9.91,9.21,16.81a18.35,18.35,0,0,1-1,5.18,1.36,1.36,0,0,0,2.22,1.42,15.74,15.74,0,0,0,5.24-11.46c0-6.38-9.4-11.54-9.4-17.12A26.73,26.73,0,0,1,59.87,22Z" style="fill: #fff;"/>
                            </svg>
                            <?php the_title(); ?>
                        </li>
                    </ul>
                </div>
                <?php the_post_thumbnail('masonry-square', array( "class" => "recipe-img" )) ?>
            </div>
            </a>
        </div>
<?php } ?>
    <?php endwhile; ?>
</div>
<?php wp_reset_postdata(); ?>

<p class="text-center"><a class="btn cfs-btn-donate" role="button" href="<?php echo get_post_type_archive_link( 'recipe' ); ?>"><?php esc_html_e('View All Recipes', 'lydia') ?></a></p>

</div>



