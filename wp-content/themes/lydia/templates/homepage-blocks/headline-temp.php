<?php
/**
*  HOME PAGE BLOCK: HEADLINE CHEFS
*  ------
*  Home page sections
*  ------
*  @package lydia
*  @since lydia 1.0
*/
?>
<div class="intro centred">
    <h2 class="title"><span class="fa fa-star"></span><br><?php esc_html_e('Recipes Coming Soon', 'lydia') ?></h2>
    <?php the_field('headline_chefs_intro') ?>
</div>
</div>
