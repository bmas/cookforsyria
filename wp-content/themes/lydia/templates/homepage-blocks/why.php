<?php
/**
*  HOME PAGE BLOCK: WHY
*  ------
*  Home page sections
*  ------
*  @package lydia
*  @since lydia 1.0
*/
?>
<div class="container-flex h-100 flex-center">
    <div class="cont-overlay">
    <h2 class="cont-overlay-title"><span class="top-title"><?php esc_html_e('Why', 'lydia') ?></span><br><?php esc_html_e('#CookForSyria', 'lydia') ?></h2>
        <?php the_field('why_text') ?>
    </div>
</div>



