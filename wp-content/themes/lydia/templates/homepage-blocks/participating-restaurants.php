<?php
/**
*  HOME PAGE BLOCK: PARTICIPATING
*  ------
*  Home page sections
*  ------
*  @package lydia
*  @since lydia 1.0
*/
?>
<div class="pattern-gradient"></div>
<div class="cont-overlay container-flex">
    <div class="grid-flex-5">
        <h2 class="sec-title"><?php esc_html_e('Participants', 'lydia') ?></h2>
    </div>
    <div class="grid-flex-7">
        <ul class="no-bullet part-rest-list">
            <?php $post_query = new WP_Query(
            array(
                'post_type' => 'participant',
                'post_status' => 'publish',
                'orderby'=> 'title',
                'order'   => 'ASC',
                'posts_per_page' => -1
                )
            );
            while ($post_query->have_posts()) : $post_query->the_post(); ?>
            <li><?php the_title(); ?></li>
            <?php endwhile; ?>
        </ul>
        <?php wp_reset_postdata(); ?>
    </div>
</div>



