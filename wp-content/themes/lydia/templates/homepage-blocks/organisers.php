<?php
/**
*  HOME PAGE BLOCK: ORGANISERS
*  ------
*  Home page sections
*  ------
*  @package lydia
*  @since lydia 1.0
*/
?>
<div class="pattern-gradient"></div>
<div class="cont-overlay container-flex">
    <div class="grid-flex-4">
    <img class="cfs-logo-long" src="<?php echo get_template_directory_uri(); ?>/img/cook-for-syria-logo-long.svg" alt="<?php esc_attr_e('Cook for Syria', 'lydia') ?>">
        <h2 class="sec-title"><?php esc_html_e('Organisers', 'lydia') ?></h2>
        <p class="photo-credit"><?php esc_html_e('Photography by', 'lydia') ?> <strong><?php the_field('organiser_photographer') ?></strong></p>
    </div>
    <div class="grid-flex-8">
        <?php if( have_rows('organisers') ): ?>
            <div class="para-list container-flex">
            <?php while( have_rows('organisers') ): the_row();
                // vars
                $name = get_sub_field('organiser_name');
                $link = get_sub_field('organiser_link');
                $bio = get_sub_field('organiser_bio');
                ?>
                <div class="grid-flex-6 para-list-item">
                    <?php if( $name ): ?>
                        <h3><a href="<?php echo $link; ?>" target="_blank"><?php echo $name; ?></a></h3>
                    <?php endif; ?>
                    <?php echo $bio; ?>
                </div>
            <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</div>



