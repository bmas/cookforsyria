<?php
/**
*  HOME PAGE BLOCK: ORDER NOW
*  ------
*  Home page sections
*  ------
*  @package lydia
*  @since lydia 1.0
*/
?>
<div>
    <div>
        <p class="text">"<?php echo strip_tags(get_field('order_now_bottom_text')); ?>"</p>

        <?php if(get_field('order_now_bottom_author')) { ?>
            <p class="author"><?php the_field('order_now_bottom_author'); ?></p>
        <?php } ?>

        <div>
        <a target="_blank" href="<?php the_field('recipe_book_url') ?>"><?php esc_html_e('Order Now', 'lydia') ?></a>
        </div>
    </div>
</div>
