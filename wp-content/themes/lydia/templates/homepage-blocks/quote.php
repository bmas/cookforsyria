<?php
/**
*  HOME PAGE BLOCK: QUOTE
*  ------
*  Home page sections
*  ------
*  @package lydia
*  @since lydia 1.0
*/
?>
<blockquote>
    <?php the_field('quote_text') ?>
    <cite><?php the_field('quote_author') ?></cite>
    <a href="<?php the_field('quote_author_link') ?>" target="_blank"><span class="fa fa-2x fa-instagram"></span></a>
</blockquote>





