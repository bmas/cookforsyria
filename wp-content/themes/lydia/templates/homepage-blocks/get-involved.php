<?php
/**
*  HOME PAGE BLOCK: GET INVOLVED
*  ------
*  Home page sections
*  ------
*  @package lydia
*  @since lydia 1.0
*/
?>
<div class="pattern-gradient"></div>
<div class="cont-overlay container-flex">
    <div class="grid-flex-4">
        <h2 class="sec-title"><?php esc_html_e('How to get involved', 'lydia') ?></h2>
        <p class="get-inv-cta"><a class="btn cfs-btn-donate" role="button" href="<?php the_field('donate_link', 'lydia_options') ?>" target="_blank"><?php esc_html_e('Donate Now', 'lydia') ?></a></p>
    </div>
    <div class="grid-flex-8">
        <?php if( have_rows('activitys') ): ?>
            <div class="para-list container-flex">
            <?php while( have_rows('activitys') ): the_row();
                // vars
                $icon = get_sub_field('activity_icon');
                $name = get_sub_field('activity_name');
                $desc = get_sub_field('activity_description');
                ?>
                <div class="grid-flex-6 para-list-item">
                    <?php if( $name ): ?>
                        <h3>
                            <img class="para-icon" src="<?php echo get_template_directory_uri().'/img/'.$icon.'.svg'; ?>" alt="<?php echo $name; ?>">
                            <?php echo $name; ?>
                        </h3>
                    <?php endif; ?>
                    <?php echo $desc; ?>
                </div>
            <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</div>



