<?php
/**
*  HEAD
*  ------
*  Head Template
*  ------
*  @package lydia
*  @since lydia 1.0
*/
?>
<!DOCTYPE html>
<html lang="en-GB" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable= no">
    <meta name="format-detection" content="telephone=no">
    <meta name="author" content="<?php bloginfo('name'); ?>" />
    <meta name="web_author" content="BMAS | bmas.agency" />
<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<?php get_template_part('inc/client-head'); ?>

<?php wp_head(); ?>

</head>
