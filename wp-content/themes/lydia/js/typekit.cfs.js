/**
*  Typekit
*  ------
*  Enqueue Typekit Font loader for wp_head
*  ------
*  @package Lydia
*  @since Lydia 1.0
*/

<script src="https://use.typekit.net/zij1rvl.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
