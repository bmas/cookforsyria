/**
*  LYDIA jQuery Functions
*  ------
*  Frontend Functions for Lydia, includes common ui element functionality
*  ------
*  @package Lydia
*  @since Lydia 1.0
*  @requires jQuery 1.11
*/

jQuery(document).ready(function($) {


    // Mobile Hamburger Menu
    $(function() {
      $('.hamburger').click(function(event) {
        event.preventDefault();
        $('.masthead').toggleClass('menu-active');
      });
    });


    // Back to top link
    $('.back-top').click(function(){
        $('html, body').animate({scrollTop:0}, 'fast');
        return false;
    });

    // Smooth Scrolling on Anchor links
    function smoothScroll(target) {
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
    $(function() {
      if (location.pathname === '/' || /cookforsyria/.test(location.pathname)) {
         var target;
         if (location.search) {
            target = $('#' + location.search.slice(1));
            $('.current-menu-item').removeClass('current-menu-item');
            smoothScroll(target);
            window.history.replaceState(null, document.title, '/');
          }
      $('a[href*="?"]:not([href="?"])').click(function(e) {
        e.preventDefault();
        target = $('#' + this.href.slice(this.href.lastIndexOf('/') + 2));
        smoothScroll(target);
      });
      }
    });

    //FAQ accordian
    $( function() {
      $( "#accordion" ).accordion({
        heightStyle: "content"
      });
    } );

    // Background fix IE
    if(navigator.userAgent.match(/Trident\/7\./)) { // if IE
      $('body').on("mousewheel", function () {
          // remove default behavior
          event.preventDefault();

          //scroll without smoothing
          var wheelDelta = event.wheelDelta;
          var currentScrollPosition = window.pageYOffset;
          window.scrollTo(0, currentScrollPosition - wheelDelta);
      });
    }

    function isLocalStorageNameSupported() {
        var testKey = 'test', storage = window.localStorage;

        try {
            storage.setItem(testKey, '1');
            storage.removeItem(testKey);
            return true;
        } catch (error) {
            return false;
        }
    }

    if(isLocalStorageNameSupported()) {
        var hours = 672;
        var now = new Date().getTime();
        var setupTime = localStorage.setupTime;

        if (setupTime == null) {
            localStorage.popupTime = now;
        } else {
            if(now - setupTime > hours*60*60*1000) {
                localStorage.closedPopup = true;
                localStorage.setupTime = now;
            }
        }
    }

    if (isLocalStorageNameSupported() && typeof localStorage != "undefined" && localStorage.closedPopup !== "false") {
        // Modal Window
        // Get the modal
        var modal = document.getElementById('modal-signup');
        // Get the button that opens the modal
        //var btn = document.getElementsByClassName("modal-open")[0];
        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("modal-close")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.visibility = "hidden";
            window.localStorage.setItem("closedPopup", "1");
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.visibility = "hidden";
                window.localStorage.setItem("closedPopup", "1");
            }
        }

        // if (btn) { btn.onclick = function() {
        //       modal.style.visibility = "visible";

        //       // save the state into session storage
        //       if (window.localStorage) {
        //           window.localStorage.setItem("closedPopup", "1")
        //       }
        //   }
        // }

        // if the modal was closed once, don't show it
        if (window.localStorage && !window.localStorage.getItem("closedPopup")) {
            modal.style.visibility = "visible";
        }

        // Close Modal on Submit
        $('form.newsletter').submit(function() {
           modal.style.visibility = "hidden";
        });

      }




    // Recipe Carousel

    // Single Fade Slider
    $(".post-slider-single-fade").owlCarousel({
        loop: false,
        navSpeed: 1600,
        autoplay: true,
        mouseDrag: true,
        items: 1,
        margin: 0,
        nav: false,
        dots: false,
        autoWidth: false,
    });
    
    if($('.filters').length > 0) {
      
      function filterPosts() {
        $('.headline-card').each(function() {
          var $post = $(this);
          var valid = true;
          var search = $('input[name="search-filter"]').val().toLowerCase();
          
          $('.filters button.active:not([data-name="all"])').each(function() {
            var $filter = $(this);
            
            if($post.data($filter.data('filter')).indexOf($filter.data('name')) == -1) {
              valid = false;
            }
          });
          
          if($post.data('title').indexOf(search) == -1) {
            valid = false;
          }
          
          if(valid) {
            $post.removeClass('hidden');
          } else {
            $post.addClass('hidden');
          }
        });
      }
      
      $('.filters button').click(function() {
        var $this = $(this);
        var wasActive = $this.hasClass('active');
        
        $('.filters button[data-filter="' + $this.data('filter') + '"]').removeClass('active');
        
        if($this.data('filter') == 'courses') {
          $('.filters button[data-name="all"]').removeClass('active');
        }
        
        if(wasActive) {
          $this.removeClass('active');
        } else {
          $this.addClass('active');
        }
        
        filterPosts();
      });
      
      $('input[name="search-filter"]')
        .keyup(filterPosts)
        .change(filterPosts);
      
    }

});


