// WP Ajax - By Will Busby - [will@bmas.agency]

function wpAjax(url) {
    var self = this;
    this.get = function(args) {
        var args = args || {};
        var action = args.action || false;
        var queries = args.queries || false;
        var success = args.success;
        var afterLoad = args.afterLoad || function() {};
        // create and begin the request to the server
        var request = new XMLHttpRequest();


        if (action !== false) {
            url = url + '?action=' + action;
            if (queries != false) {
                for (prop in queries) {
                    url = url + '&' + prop + '=' + queries[prop];
                }
            }
        }
        if (queries != false && action === false) {
            url = url + '?';
            for (prop in queries) {
                url = url + '&' + prop + '=' + queries[prop];
            }
        }
        request.open('GET', url, true);
        request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        // what to do when the request works
        request.onload = function() {
            if (request.status >= 200 && request.status < 400) {
                // we reached the server and was successful. The function param runs here.
                console.log("yay! Request was successful");

                data = request.responseText;
                success(data);
            } else {
                // we reached our target server, but it returned an error
                console.log("Error after the connection");
            }
        };

        request.onloadend = function() {
            afterLoad();
        }

        // when request couldn't reach server
        request.onerror = function(err) {
            data = err;
            console.log(data);
        };
        request.send();
    }
}
