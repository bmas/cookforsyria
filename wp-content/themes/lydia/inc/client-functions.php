<?php
/**
*  CLIENT
*  ------
*  Additional functions as per the client site requirements
*  ------
*  @package Lydia
*  @since Lydia 1.0
*/

// Main Menu Home and Search Wrap
// function lydia_nav_home_wrap() {
//   // default value of 'items_wrap' is <ul id="%1$s" class="%2$s">%3$s</ul>'
//   // open the <ul>, set 'menu_class' and 'menu_id' values
//   $wrap  = '<ul class="menu container-flex wrapper no-bullet">';
//   // the static link
//   $wrap .= '<li id="nav-home-icon"><a class="fa fa-home" href="'. home_url() .'"></a></li>';
//   // get nav items as configured in /wp-admin/
//   $wrap .= '%3$s';
//   // close the <ul>
//   $wrap .= '</ul>';
//   // return the result
//   return $wrap;
// }

// Google Maps ACF Fix

add_filter('acf/settings/google_api_key', function () {
    return 'AIzaSyDQpuwyZecDAwmdc2l0pjPsQprkE8SUz_c';
});
