<?php
/**
*  Head Client Additions
*  ------
*  Additional Client Scripts or Non standard additions to the head of the site such as trackers or font scripts.
*  ------
*  @package lydia
*  @since lydia 1.0
*/
?>

<!-- Typekit -->
<script src="https://use.typekit.net/zij1rvl.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
