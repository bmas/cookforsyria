<?php
/**
*  CUSTOM POST TYPE AND TAXONOMIES
*  ------
*  Register Custom Post types and taxonomies
*  Taxonomies must be registered before post types
*  ------
*  @package Lydia
*  @since Lydia 1.0
*/

// CUSTOM TAXONOMIES
function lydia_custom_taxonomy() {

    $labels = array(
        'name'                       => _x( 'Courses', 'Taxonomy General Name', 'lydia' ),
        'singular_name'              => _x( 'Course', 'Taxonomy Singular Name', 'lydia' ),
        'menu_name'                  => __( 'Courses', 'lydia' ),
        'all_items'                  => __( 'All courses', 'lydia' ),
        'parent_item'                => __( 'Parent course', 'lydia' ),
        'parent_item_colon'          => __( 'Parent course:', 'lydia' ),
        'new_item_name'              => __( 'New course Name', 'lydia' ),
        'add_new_item'               => __( 'Add New course', 'lydia' ),
        'edit_item'                  => __( 'Edit course', 'lydia' ),
        'update_item'                => __( 'Update course', 'lydia' ),
        'view_item'                  => __( 'View course', 'lydia' ),
        'separate_items_with_commas' => __( 'Separate items with commas', 'lydia' ),
        'add_or_remove_items'        => __( 'Add or remove courses', 'lydia' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'lydia' ),
        'popular_items'              => __( 'Popular courses', 'lydia' ),
        'search_items'               => __( 'Search courses', 'lydia' ),
        'not_found'                  => __( 'Not Found', 'lydia' ),
        'no_terms'                   => __( 'No courses', 'lydia' ),
        'items_list'                 => __( 'Courses list', 'lydia' ),
        'items_list_navigation'      => __( 'Courses list navigation', 'lydia' ),
    );
    $rewrite = array(
        'slug'                       => 'course',
        'with_front'                 => true,
        'hierarchical'               => false,
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => false,
        'rewrite'                    => $rewrite,
    );
    register_taxonomy( 'course', array( 'recipe' ), $args );

}
add_action( 'init', 'lydia_custom_taxonomy', 0 );

// REGISTER CUSTOM POST TYPE
function lydia_custom_post() {

    // Recipes
    $labels = array(
        'name'                => _x( 'Recipes', 'Post Type General Name', 'lydia' ),
        'singular_name'       => _x( 'Recipe', 'Post Type Singular Name', 'lydia' ),
        'menu_name'           => __( 'Recipes', 'lydia' ),
        'name_admin_bar'      => __( 'Recipes', 'lydia' ),
        'parent_item_colon'   => __( 'Parent recipe:', 'lydia' ),
        'all_items'           => __( 'All recipes', 'lydia' ),
        'add_new_item'        => __( 'Add New recipe', 'lydia' ),
        'add_new'             => __( 'Add New', 'lydia' ),
        'new_item'            => __( 'New recipe', 'lydia' ),
        'edit_item'           => __( 'Edit recipe', 'lydia' ),
        'update_item'         => __( 'Update recipe', 'lydia' ),
        'view_item'           => __( 'View recipe', 'lydia' ),
        'search_items'        => __( 'Search recipe', 'lydia' ),
        'not_found'           => __( 'No recipe found', 'lydia' ),
        'not_found_in_trash'  => __( 'No recipe found in Trash', 'lydia' ),
    );
    $args = array(
        'label'               => __( 'Recipe', 'lydia' ),
        'description'         => __( 'Recipe Posts', 'lydia' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', 'page-attributes'),
        'taxonomies'          => array( 'course' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-carrot',
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'rewrite'             => array( 'slug' => 'recipe', 'with_front' => false)
    );
    register_post_type( 'recipe', $args );

    // Participants
    $labels = array(
        'name'                => _x( 'Participants', 'Post Type General Name', 'lydia' ),
        'singular_name'       => _x( 'Participant', 'Post Type Singular Name', 'lydia' ),
        'menu_name'           => __( 'Participants', 'lydia' ),
        'name_admin_bar'      => __( 'Participants', 'lydia' ),
        'parent_item_colon'   => __( 'Parent participant:', 'lydia' ),
        'all_items'           => __( 'All participants', 'lydia' ),
        'add_new_item'        => __( 'Add New participant', 'lydia' ),
        'add_new'             => __( 'Add New', 'lydia' ),
        'new_item'            => __( 'New participant', 'lydia' ),
        'edit_item'           => __( 'Edit participant', 'lydia' ),
        'update_item'         => __( 'Update participant', 'lydia' ),
        'view_item'           => __( 'View participant', 'lydia' ),
        'search_items'        => __( 'Search participant', 'lydia' ),
        'not_found'           => __( 'No participant found', 'lydia' ),
        'not_found_in_trash'  => __( 'No participant found in Trash', 'lydia' ),
    );
    $args = array(
        'label'               => __( 'Participant', 'lydia' ),
        'description'         => __( 'Participant Posts', 'lydia' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', 'page-attributes'),
        //'taxonomies'          => array( '' ),
        'hierarchical'        => false,
        'public'              => false, // Does not have a dedicated URL
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-groups',
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'capability_type'     => 'post',
        'rewrite'             => array( 'slug' => 'participant', 'with_front' => false)
    );
    register_post_type( 'participant', $args );

}
add_action( 'init', 'lydia_custom_post', 0 );
