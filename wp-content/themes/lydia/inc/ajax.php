<?php

add_action( 'wp_ajax_call_ajax', 'lydia_call_ajax' );
add_action( 'wp_ajax_nopriv_call_ajax', 'lydia_call_ajax' );

function lydia_call_ajax() {
    $offset = $_GET['offset'];
    $pt = $_GET['post_type'];
    $catName = $_GET['cat_name'];
    $catValue = $_GET['cat_value'];
	$meta = false;
	if (!empty($search_f) || !empty($loc)) {
		if (!empty($search_f)) {
			$search_f = str_replace(" ", "", explode(',', $search_f));
		} else {
			$search_f = explode(",", $loc);
		}
		$meta = true;
	}
    $post_query = new WP_Query(
        array(
            'post_type' => $pt,
            'offset'	=> $offset,
            $catName    => $catValue,
            'post_status' => 'publish',
            'posts_per_page' => 6
			)
        );
    while ($post_query->have_posts()) : $post_query->the_post();
?>

<article class="post-block bar grid-flex-4 ajax-post animated">
    <div class="cont-rel">
        <figure class="entry-image">
            <a class="entry-link" href="<?php the_permalink(); ?>">
                <?php
                if ( has_post_thumbnail() ) {
                    the_post_thumbnail( 'featured-thumb' );
                } elseif( lydia_first_post_image() ) { // Set the first image from the editor
                    echo '<img src="' . lydia_first_post_image() . '" class="wp-post-image" />';
                } ?>
            </a>
        </figure>
        <header class="entry-header">
            <div class="entry-meta"></div>
            <h2 class="entry-title">
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </h2>
        </header>
    </div>
</article>

<?php endwhile;
die();
}
