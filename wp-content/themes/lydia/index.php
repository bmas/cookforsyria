<?php
/**
*  INDEX
*  ------
*  Primary Template
*  ------
*  @package lydia
*  @since lydia 1.0
*/
get_header(); ?>

<main class="raised">
    <article class="primary-cont">
        <h1 class="sec-title"><?php the_title() ?></h1>
        <?php the_content() ?>
    </article>
</main>

<?php get_footer() ?>
