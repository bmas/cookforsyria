<?php
/**
*  HEADER
*  ------
*  Primary Header Template
*  ------
*  @package lydia
*  @since lydia 1.0
*/
?>
<?php get_template_part('templates/head'); ?>

<body id="main-view" <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
    <header class="container masthead" itemscope itemtype="http://schema.org/WPHeader">
        <div class="main-logo grid-flex-3" itemscope itemtype="http://schema.org/Organization">
            <a itemprop="url" rel="home" href="<?php echo home_url();?>">
                <img src="<?php echo get_template_directory_uri(); ?>/img/cook-for-syria-logo-long.svg" alt="<?php esc_attr_e('Cook for Syria', 'lydia') ?>">
            </a>
        </div>


    <?php wp_nav_menu( array(
            'theme_location'  => 'header',
            'menu'            => 'header',
            'container'       => 'nav',
            'container_class' => 'header-menu grid-flex-9',
            //'items_wrap'      => lydia_nav_home_wrap()
        ));
    ?>

    <svg role="navigation" class="hamburger" width="23" height="23" viewBox="0 0 46 30">
        <path id="hamburger-top" d="M1,0h44c0.6,0,1,0.4,1,1v4c0,0.6-0.4,1-1,1H1C0.4,6,0,5.6,0,5V1C0,0.4,0.4,0,1,0z"></path>
        <path id="hamburger-middle" d="M1,12h44c0.6,0,1,0.4,1,1v4c0,0.6-0.4,1-1,1H1c-0.6,0-1-0.4-1-1v-4C0,12.4,0.4,12,1,12z"></path>
        <path id="hamburger-bottom" d="M1,24h44c0.6,0,1,0.4,1,1v4c0,0.6-0.4,1-1,1H1c-0.6,0-1-0.4-1-1v-4C0,24.4,0.4,24,1,24z"></path>
    </svg>

    </header>




