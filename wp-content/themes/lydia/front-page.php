<?php
/**
*  FRONT PAGE
*  ------
*  Home Page template
*  ------
*  @package lydia
*  @since lydia 1.0
*/
get_header(); ?>

<main>
    <section id="intro" style="background-image: url(<?php echo the_post_thumbnail_url('full');?>)">
       <?php get_template_part('templates//homepage-blocks/intro'); ?>
    </section>

<!--     <section id="recipe-book">
        <h2>#COOKFORSYRIA: THE RECIPE BOOK</h2>
        <p>Now available to order, be the first get yourself a copy<br> and discover the Syrian inspired dishes from the #CookForSYRIA campaign.</p>
        <p><a href="https://www.amazon.co.uk/Cook-Syria-Recipe-Book/dp/1527203344/" target="_blank" class="btn text-center">Order Now</a></p>
    </section> -->

    <section id="quote">
        <?php get_template_part('templates//homepage-blocks/quote'); ?>
    </section>

    <section id="what" style="background-image: url(<?php the_field('what_background');?>)">
        <?php get_template_part('templates//homepage-blocks/what'); ?>
    </section>

    <section id="organisers" class="pattern" style="background-image: url(<?php echo get_template_directory_uri();?>/img/star-tile-pattern-blue.svg)">
        <?php get_template_part('templates//homepage-blocks/organisers'); ?>
    </section>

    <section id="why" style="background-image: url(<?php the_field('why_background');?>)">
        <?php get_template_part('templates//homepage-blocks/why'); ?>
    </section>

    <section id="get-involved" class="pattern" style="background-image: url(<?php echo get_template_directory_uri();?>/img/star-tile-pattern-grey.svg);">
        <?php get_template_part('templates//homepage-blocks/get-involved'); ?>
    </section>

    <section id="headline-chefs" style="background-image: url(<?php the_field('headline_chefs_background');?>)">
        <?php get_template_part('templates//homepage-blocks/headline-chefs'); ?>
    </section>

    <section id="participating-restaurants" class="pattern" style="background-image: url(<?php echo get_template_directory_uri();?>/img/star-tile-pattern-blue.svg);">
        <?php get_template_part('templates//homepage-blocks/participating-restaurants'); ?>
    </section>

    <section id="order-now" style="background-image: url(<?php the_field('order_now_bottom_background') ?>);">
        <?php get_template_part('templates//homepage-blocks/order-now'); ?>
    </section>
</main>

<?php get_footer() ?>

