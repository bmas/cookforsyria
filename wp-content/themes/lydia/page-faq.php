<?php
/**
*  FAQ PAGE
*  ------
*  Primary Template
*  ------
*  @package lydia
*  @since lydia 1.0
*/
get_header(); ?>

<main class="raised">
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
    <article class="primary-cont">
        <div class="container-flex">
            <div class="grid-flex-5">
                <h1 class="sec-title"><?php the_title() ?></h1>
                <?php echo the_content() ?>
            </div>
            <div class="grid-flex-7">

                <?php if( have_rows('faqs') ): ?>
                    <div class="faq-list" id="accordion">
                    <?php while( have_rows('faqs') ): the_row();
                        // vars
                        $question = get_sub_field('faq_question');
                        $answer = get_sub_field('faq_answer');
                        ?>
                            <h2 class="faq-answer"><?php echo $question ?></h2>
                            <div class="faq-question">
                                <?php echo $answer ?>
                            </div>
                    <?php endwhile; ?>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </article>
    <?php endwhile; ?>
    <?php else : ?>
        <article class="primary-cont">
            <p><?php esc_html_e('No Content', 'lydia') ?></p>
        </article>
<?php endif; ?>
</main>


<?php get_footer() ?>
