<?php
use Nette\Caching\Cache;
use Nette\Caching\Storages\FileStorage;

class instagram {
protected $instagram, $cacheDir;

	function __construct($cacheDir, $user_id, $access_token) {
		$this->cacheDir = $cacheDir;
		$this->instagram = [
			'userId' => $user_id,
			'accessToken' => $access_token,
			'apiCallback' => 'none'
		];
	}

	function loadFeed($user_id, $access_token, $count = 0) {
		if (!is_dir($this->cacheDir)) {
			mkdir($this->cacheDir, 0777, true);
		}
		$instagram = $this->instagram;
		$storage = new FileStorage($this->cacheDir);
		$cache = new Cache($storage);
		try {
			$media = $cache->load('instagramFeed', function(&$dependencies) use ($user_id, $access_token, $count) {
				$dependencies = [Cache::EXPIRE => '10 minutes'];
				$json_link = "https://api.instagram.com/v1/users/{$user_id}/media/recent/?";
				$json_link.= "access_token={$access_token}&count={$count}";

				$json = file_get_contents($json_link);
				$obj = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);
				return $obj;
			});
		} catch (Exception $e) {
			return [];
		}
		return $media;
	}
}
